from board import Board
import math


class Minimax_V1:

    def __init__(self, _alpha=-math.inf, _beta=math.inf):

        self.alpha = _alpha
        self.beta = _beta

    def callNextMove(self, node, depth, color):

        maximizingPlayer = True
        self.chooseDecisionChild(node, depth, maximizingPlayer, color)

        if node.board.win(color) == True:

            return None, None

        return node.getDecisionChild().getFromCell(), node.getDecisionChild().getToCell()

    def chooseDecisionChild(self, node, depth, maximizingPlayer, color):

        if depth == 0:

            node.setEvaluationFunction(color)
            return node.utility

        decisionChild = None
        if maximizingPlayer:

            maxVal = -math.inf

            for child in node.children:

                _eval = self.chooseDecisionChild(
                    child, depth - 1, False, color)

                if _eval > maxVal:

                    maxVal = _eval
                    decisionChild = child

                self.alpha = max(self.alpha, _eval)

                if self.beta <= self.alpha:

                    break

            node.setUtility(maxVal)
            node.setDecisionChild(decisionChild)
            return maxVal

        minVal = math.inf

        for child in node.children:

            _eval = self.chooseDecisionChild(
                child, depth - 1, True, color)

            if _eval < minVal:

                minVal = _eval
                decisionChild = child

            self.beta = min(self.beta, _eval)

            if self.beta <= self.alpha:

                break

        node.setUtility(minVal)
        node.setDecisionChild(decisionChild)
        return minVal
