
from minimax_V1 import Minimax_V1
from tree_V1 import Tree


class Agent:
    def __init__(self, color, oppopnentColor, time=None):
        self.color = color
        self.opponentColor = oppopnentColor
        self.height = 4

    def move(self, board, color):
        gameTree = Tree(board, self.color, self.opponentColor, self.height)
        a = Minimax_V1()
        from_cell, to_cell = a.callNextMove(
            gameTree.root, self.height, color)
        return from_cell, to_cell
