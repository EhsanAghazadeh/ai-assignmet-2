from RandomMinimaxAgent import RandomMinimaxAgent
from agent import Agent
from board import Board
from graphicalBoard import GraphicalBoard
from agent_V1 import RandomMinimaxAgent_1
from agent_S import Agent_1


def switchTurn(turn):
    if turn == 'W':
        return 'B'
    return 'W'


import time


def play(white, black, board):
    graphicalBoard = GraphicalBoard(board)
    turn = 'W'
    while not board.finishedGame():
        if turn == 'W':
            from_cell, to_cell = white.move(board)

            if from_cell == None:

                break
        elif turn == 'B':

            start_time = time.time()
            from_cell, to_cell = black.move(board, 'B')
            print("--- %s seconds ---" % (time.time() - start_time))

            if from_cell == None:

                break
        else:
            raise Exception
        board.changePieceLocation(turn, from_cell, to_cell)
        turn = switchTurn(turn)
        graphicalBoard.showBoard()


start_time = time.time()

if __name__ == '__main__':
    board = Board(6, 6, 2)
    white = RandomMinimaxAgent('W', 'B')
    you = Agent('B', 'W')
    play(white, you, board)

print("--- %s seconds ---" % (time.time() - start_time))
