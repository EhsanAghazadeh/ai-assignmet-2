

from random import randint
from board import Board


class Node:
    def __init__(self, from_cell, to_here_cell, board):
        self.children = []
        self.utility = 0
        self.decisionChild = None
        self.from_cell = from_cell
        self.to_here_cell = to_here_cell
        self.board = board

    def setChild(self, child):
        self.children.append(child)

    def setUtility(self, utility):
        self.utility = utility

    def setDecisionChild(self, decisionChild):
        self.decisionChild = decisionChild

    def getDecisionChild(self):
        return self.decisionChild

    def getFromCell(self):
        return self.from_cell

    def getToCell(self):
        return self.to_here_cell

    def setEvaluationFunction(self, color):

        evaluation = None

        if color == 'B':
            evaluation = self.board.getNumberOfArmy(color) - 0.5 * self.board.getNumberOfArmy(
                'W') + self.board.getLastSoldierRow(color)**2 - 5 * self.board.getLastSoldierRow('W')**2

        else:

            evaluation = self.board.getNumberOfArmy(color) - 0.5 * self.board.getNumberOfArmy(
                'B') + self.board.getLastSoldierRow(color)**2 - 5 * self.board.getLastSoldierRow('B')**2

        self.utility = evaluation
